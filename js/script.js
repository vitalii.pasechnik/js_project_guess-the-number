const startBtn = document.querySelector('.btn__start');
const increment = document.querySelector('.btn__inc');
const decrement = document.querySelector('.btn__dec');
const text = document.querySelector('.text');
const num = document.querySelector('.number');
const display = document.querySelector('.display');
const finish = document.querySelector('.btn__finish');

let result;
let resultMax;
let resultMin;
let count = 0;

startBtn.onclick = () => {
	display.style.display = "flex";
	increment.style.display = "inline-block";
	decrement.style.display = "inline-block";
	finish.style.display = "block";
	text.innerHTML = "Вы загадали число";
	result = Math.floor(Math.random() * 101);
	num.innerHTML = `${result}?`;

	startBtn.classList.remove("active");
	startBtn.setAttribute('disabled', true);

	increment.classList.add("active");
	increment.removeAttribute('disabled');

	decrement.classList.add("active");
	decrement.removeAttribute('disabled');

	finish.classList.add("active");
	finish.removeAttribute('disabled');

	resultMax = 100;
	resultMin = 0;
	checkResult();
	count++;
	// console.log("MAX", resultMax);
	// console.log("MIN", resultMin);
}

decrement.onclick = () => {
	count++;
	if (resultMax === 1) {
		final();
	} else {
		resultMax = result - 1;
		result = randomizer(result);
		num.innerHTML = `${result}?`;
		checkResult();

		// console.log("result", result);
		// console.log("MAX", resultMax);
		// console.log("MIN", resultMin);
	}
};

increment.onclick = () => {
	count++;
	if (resultMin === 100) {
		final();
	} else {
		resultMin = result + 1;
		result = randomizer(result);
		num.innerHTML = `${result}?`;
		checkResult();

		// console.log("result", result);
		// console.log("MIN", resultMin);
		// console.log("MAX", resultMax);
	}
};

finish.onclick = () => final();

function randomizer(oldRandom) {
	let newRandom;
	do {
		newRandom = Math.floor(Math.random() * (resultMax - resultMin) + resultMin);
	} while (newRandom === oldRandom)
	return newRandom;
}

function checkResult() {
	if (result === resultMin) {
		decrement.classList.remove("active");
		decrement.setAttribute('disabled', true);
		decrement.innerHTML = "Меньше нельзя"
	} else {
		decrement.classList.add("active");
		decrement.removeAttribute('disabled');
		decrement.innerHTML = "Меньше --"
	}

	if (result === resultMax) {
		increment.classList.remove("active");
		increment.setAttribute('disabled', true);
		increment.innerHTML = "Больше нельзя"
	} else {
		increment.classList.add("active");
		increment.removeAttribute('disabled');
		increment.innerHTML = "Больше +"
	}

	if (resultMax === resultMin) {
		final();
	}
}

function final() {
	text.innerHTML = `Ура! Я угадал ваше число за ${count} попыток`;
	num.innerHTML = `${result}!`;
	startBtn.innerHTML = `Начать заново!`;

	finish.setAttribute('disabled', true);
	finish.classList.remove("active");

	startBtn.classList.add("active");
	startBtn.removeAttribute('disabled');

	increment.classList.remove("active");
	increment.setAttribute('disabled', true);

	decrement.classList.remove("active");
	decrement.setAttribute('disabled', true);
	
	count = 0;
}



